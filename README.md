# README #

## Movie-Info (Slack app) ##

It is a slack application that will display an App Home tab with a button that will allow users to select a movie from a list and get a DM with some information about the film. The movie information will be pulled from The Movie Database API.

### Built With ###
**App generated using Spring Initializr.**
* Bolt
* Spring-Boot
* JUnit5
* Hamcrest
* Maven
* Lombok


## Running locally ##

To run the app locally follow the steps below:

1. Add env variables 
   SLACK_BOT_TOKEN = <<app_bot_token_value>>
   SLACK_SIGNING_SECRET = <<slack_signing_secret>>


2. Run `ngrok http 3000` on terminal to start ngrok. It creates a tunnel between slack api and java app.
    * In case you do not have ngrok installed follow check
      [Using ngrok to develop locally for Slack](https://api.slack.com/tutorials/tunneling-with-ngrok).


3. Running
    * Maven
        * ``mvn exec:java -Dexec.mainClass="com.movie.info.MovieInfoApplication"``
    * Intellij Idea
        * Edit Run/Debug configuration for MovieInfoApplication and add the env variables mentioned in the step 1.
        * Run it. shortcut: ``Ctrl + R``
