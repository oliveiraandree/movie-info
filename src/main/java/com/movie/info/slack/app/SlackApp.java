package com.movie.info.slack.app;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.movie.info.slack.app.components.DirectMessageComponent;
import com.movie.info.slack.app.components.ExternalSelectComponent;
import com.movie.info.slack.app.components.HomeTabComponent;
import com.movie.info.slack.app.components.ModalComponent;
import com.slack.api.bolt.App;

/**
 * The type Slack app.
 */
@Log4j2
@Configuration
@RequiredArgsConstructor
public class SlackApp {

  private final DirectMessageComponent directMessageComponent;

  private final ExternalSelectComponent externalSelectComponent;

  private final HomeTabComponent homeTabComponent;

  private final ModalComponent modalComponent;

  /**
   * This is an essential part of this application. All the logic to handle Slack events should be here. In this
   * @Configuration class, you can also inject service classes and whatever into Bolt’s App by taking full advantage of
   * the Spring DI container.
   */
  @Bean
  public App initSlackApp() {
    log.info("Configuring Movie-Info slack app.");
    App app = new App();
    homeTabComponent.createHomeTab(app);
    modalComponent.createModal(app);
    externalSelectComponent.createTypeAhead(app);
    directMessageComponent.sendDirectMessage(app);
    return app;
  }
}
