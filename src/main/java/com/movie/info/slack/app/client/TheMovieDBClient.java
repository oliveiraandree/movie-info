package com.movie.info.slack.app.client;

import info.movito.themoviedbapi.model.MovieDb;

import java.util.List;

public interface TheMovieDBClient {

  List<MovieDb> loadMovies(String input);

  MovieDb getMovieDetails(Integer movieId);
}
