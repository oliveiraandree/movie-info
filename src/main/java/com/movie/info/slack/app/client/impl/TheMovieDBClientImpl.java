package com.movie.info.slack.app.client.impl;

import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.movie.info.slack.app.client.TheMovieDBClient;

/**
 * The Movie Database (TMDB) - client responsible to talk to TMDB Api
 */

@Log4j2
@Component
@RequiredArgsConstructor
public class TheMovieDBClientImpl implements TheMovieDBClient {

  private static final String API_KEY = "6226413efc4ed69bdd346ce7237c8efe";

  private final WebClient webClient;

  /**
   * Load movies which match the input
   *
   * @param input
   * @return a json with a list of movies which match the search.
   */
  @Override
  public List<MovieDb> loadMovies(String input) {
    MovieResultsPage response =
      webClient.get().uri("/search/movie", uriBuilder -> uriBuilder.queryParam("api_key", API_KEY)
        .queryParam("query", input).build()).retrieve()
        .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
          final String message = String
            .format("Client error received from TMDB with status code {%s}", clientResponse.statusCode());
          log.error(message);
          return Mono.empty();
        })
        .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
          final String message = String
            .format("Server Error, unsuccessful call to TMDB with status code {%s}", clientResponse.statusCode());
          log.error(message);
          return Mono.empty();
        })
        .bodyToMono(MovieResultsPage.class).block();
    log.info("Total of movies: " + Objects.requireNonNull(response).getResults().size());
    return response.getResults();
  }

  @Override
  public MovieDb getMovieDetails(Integer movieId) {
    MovieDb response =
      webClient.get().uri("/movie/{movieId}", uriBuilder -> uriBuilder.queryParam("api_key", API_KEY)
        .build(movieId)).retrieve()
        .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
          final String message = String
            .format("Client error received from TMDB with status code {%s}", clientResponse.statusCode());
          log.error(message);
          return Mono.empty();
        })
        .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
          final String message = String
            .format("Server Error, unsuccessful call to TMDB with status code {%s}", clientResponse.statusCode());
          log.error(message);
          return Mono.empty();
        })
        .bodyToMono(MovieDb.class).block();
    log.info("Movie detail: " + response);
    return response;
  }
}
