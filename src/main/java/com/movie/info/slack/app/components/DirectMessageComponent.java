package com.movie.info.slack.app.components;

import static com.movie.info.slack.app.constants.Constants.DIRECT_MESSAGE_TEMPLATE;
import static com.movie.info.slack.app.constants.Constants.DM_HEADER;
import static com.movie.info.slack.app.constants.Constants.IMAGE_URL_BASE;
import static com.slack.api.model.block.Blocks.asBlocks;
import static com.slack.api.model.block.Blocks.header;
import static com.slack.api.model.block.Blocks.section;
import static com.slack.api.model.block.composition.BlockCompositions.markdownText;
import static com.slack.api.model.block.composition.BlockCompositions.plainText;
import static com.slack.api.model.block.element.BlockElements.image;

import info.movito.themoviedbapi.model.MovieDb;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

import com.movie.info.slack.app.service.MovieService;
import com.slack.api.bolt.App;
import com.slack.api.bolt.context.builtin.ViewSubmissionContext;
import com.slack.api.bolt.request.builtin.ViewSubmissionRequest;
import com.slack.api.methods.SlackApiException;
import com.slack.api.model.block.composition.MarkdownTextObject;
import com.slack.api.model.block.element.ImageElement;
import com.slack.api.model.view.ViewState;

/**
 * The type Direct message component.
 */
@Log4j2
@Component
@RequiredArgsConstructor
public class DirectMessageComponent {

  /**
   * The constant DROPDOWN_LIST_ID.
   */
  public static final String DROPDOWN_LIST_ID = "movies-select";

  private final MovieService movieService;

  /**
   * Send direct message.
   *
   * @param app the app
   */
  public final void sendDirectMessage(App app) {
    log.info("Registering direct message component.");
    // action for confirm button
    app.viewSubmission("", (ViewSubmissionRequest req, ViewSubmissionContext ctx) -> {
      // retrieving selected movie
      ViewState.SelectedOption selectedOption = getSelectedOption(req);
      log.debug("Option selected: Id: {} - Movie: {}", selectedOption.getValue(), selectedOption.getText());
      // send direct message
      sendDirectMessage(req, ctx, selectedOption);
      return ctx.ack();
    });
  }

  private void sendDirectMessage(ViewSubmissionRequest req, ViewSubmissionContext ctx,
    ViewState.SelectedOption selectedOption)
    throws IOException, SlackApiException {
    MovieDb movieDetails = movieService.getDetails(Integer.parseInt(selectedOption.getValue()));

    String userId = req.getPayload().getUser().getId();
    log.debug("Creating direct message. userId: {}", userId);
    ctx.client().chatPostMessage(r -> r
      .channel(userId) // channel  to send the DM
      .blocks(asBlocks(
        // components to be sent in the DM
        section(s -> s.text(markdownText(DM_HEADER))),
        header(header -> header.text(plainText(movieDetails.getOriginalTitle()))),
        section(s -> s.text(getText(movieDetails)).accessory(getImage(movieDetails)))
      ))
    );
    log.debug("Direct message successfully sent. userId: {}", userId);
  }

  private MarkdownTextObject getText(MovieDb movieDetails) {
    return markdownText(mt -> mt.text(getDirectMessageText(movieDetails)));
  }

  private ImageElement getImage(MovieDb movieDetails) {
    return image(image -> image.imageUrl(getImageUrl(movieDetails))
      .altText(movieDetails.getOriginalTitle()));
  }

  private String getImageUrl(MovieDb movieDetails) {
    return IMAGE_URL_BASE + movieDetails.getPosterPath();
  }

  private String getDirectMessageText(MovieDb movieDetails) {

    LocalDate dateTime = LocalDate.parse(movieDetails.getReleaseDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    String msgText = DIRECT_MESSAGE_TEMPLATE
      .replace("<<RELEASE_DATE>>", dateTime.format(DateTimeFormatter.ofPattern("MMM d, yyyy")))
      .replace("<<DETAILS>>", movieDetails.getOverview());
    return msgText;
  }

  private ViewState.SelectedOption getSelectedOption(ViewSubmissionRequest req) {
    return req.getPayload().getView().getState().getValues().entrySet().stream()
      .findFirst().get().getValue().get(DROPDOWN_LIST_ID).getSelectedOption();
  }
}
