package com.movie.info.slack.app.components;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.movie.info.slack.app.service.MovieService;
import com.slack.api.bolt.App;

/**
 * The type External select component.
 */
@Log4j2
@Component
@RequiredArgsConstructor
public class ExternalSelectComponent {

  @Value("${movie.info.modal.select-movie-id}")
  private String selectMovieId;

  private final MovieService movieService;

  /**
   * Create type ahead for movies. It will invoke the TMDB api to check the movies.
   *
   * @param app the app
   */
  public void createTypeAhead(App app) {
    log.info("Registering typeahead component.");
    app.blockSuggestion(selectMovieId,
      (req, ctx) -> {
        log.debug("Loading movies for value: {}", req.getPayload().getValue());
        return ctx.ack(r -> r.options(movieService.loadMovies(req.getPayload().getValue())));
      });
  }
}
