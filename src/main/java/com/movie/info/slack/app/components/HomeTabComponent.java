package com.movie.info.slack.app.components;

import static com.slack.api.model.block.Blocks.actions;
import static com.slack.api.model.block.Blocks.asBlocks;
import static com.slack.api.model.block.Blocks.header;
import static com.slack.api.model.block.Blocks.section;
import static com.slack.api.model.block.composition.BlockCompositions.markdownText;
import static com.slack.api.model.block.composition.BlockCompositions.plainText;
import static com.slack.api.model.block.element.BlockElements.asElements;
import static com.slack.api.model.block.element.BlockElements.button;
import static com.slack.api.model.view.Views.view;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.slack.api.app_backend.events.payload.EventsApiPayload;
import com.slack.api.bolt.App;
import com.slack.api.bolt.context.builtin.EventContext;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.response.views.ViewsPublishResponse;
import com.slack.api.model.event.AppHomeOpenedEvent;
import com.slack.api.model.view.View;

/**
 * The type Movie info home event.
 */
@Log4j2
@Component
@RequiredArgsConstructor
public class HomeTabComponent {

  @Value("${movie.info.home-tab.movie-button-id}")
  private String movieButtonId;

  @Value("${movie.info.home-tab.movie-button-text}")
  private String movieButtonText;

  @Value("${movie.info.home-tab.header}")
  private String homeTabHeader;

  @Value("${movie.info.home-tab.welcome-message}")
  private String welcomeMessage;

  /**
   * add home tab handler.
   *
   * @param
   */
  public void createHomeTab(App app) {
    log.info("Registering home tab component.");
    // listening to an event AppHomeOpenedEvent
    app.event(AppHomeOpenedEvent.class, (payload, ctx) -> {
      // Build a Home tab view
      log.debug("Home tab invoked by {}", payload.getEvent().getUser());
      View appHomeView = createAppHomeView(payload);
      // Update the App Home for the given user
      appHomeResponse(payload, ctx, appHomeView);
      return ctx.ack();
    });
  }

  /**
   * Home response
   *
   * @param payload
   * @return ViewsPublishResponse
   */
  private ViewsPublishResponse appHomeResponse(EventsApiPayload<AppHomeOpenedEvent> payload, EventContext ctx,
    View appHomeView) throws IOException, SlackApiException {
    return ctx.client().viewsPublish(r -> r
      .userId(payload.getEvent().getUser())
      .view(appHomeView)
    );
  }

  /**
   * Visual components for Home tab
   *
   * @param payload
   * @return View
   */
  private View createAppHomeView(EventsApiPayload<AppHomeOpenedEvent> payload) {
    return view(view -> view
      .type("home")
      .blocks(asBlocks(
        header(header -> header.text(plainText(homeTabHeader))),
        section(section ->
          section.text(markdownText(mt ->
            mt.text(welcomeMessage.replace("<<USER_NAME>>", "<@" + payload.getEvent().getUser() + ">"))))),
        actions(asElements(button(button -> button.actionId(movieButtonId)
          .value(movieButtonId)
          .text(plainText(pt -> pt.text(movieButtonText).emoji(true)))
        )))
      ))
    );
  }
}
