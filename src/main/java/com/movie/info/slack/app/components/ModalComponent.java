package com.movie.info.slack.app.components;

import static com.slack.api.model.block.Blocks.actions;
import static com.slack.api.model.block.Blocks.asBlocks;
import static com.slack.api.model.block.Blocks.section;
import static com.slack.api.model.block.composition.BlockCompositions.markdownText;
import static com.slack.api.model.block.composition.BlockCompositions.plainText;
import static com.slack.api.model.block.element.BlockElements.asElements;
import static com.slack.api.model.block.element.BlockElements.externalSelect;
import static com.slack.api.model.view.Views.view;
import static com.slack.api.model.view.Views.viewClose;
import static com.slack.api.model.view.Views.viewSubmit;
import static com.slack.api.model.view.Views.viewTitle;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.movie.info.slack.app.constants.Constants;
import com.slack.api.bolt.App;
import com.slack.api.methods.SlackApiException;
import com.slack.api.model.block.composition.PlainTextObject;

/**
 * The type Modal component.
 */
@Log4j2
@Component
public class ModalComponent {

  @Value("${movie.info.modal.title}")
  private String modalTitle;

  @Value("${movie.info.home-tab.movie-button-id}")
  private String movieButtonId;

  @Value("${movie.info.modal.confirm-button-text}")
  private String confirmButtonText;

  @Value("${movie.info.modal.close-button-text}")
  private String closeButtonText;

  @Value("${movie.info.modal.select-movie-label}")
  private String selectMovieLabel;

  @Value("${movie.info.modal.select-movie-id}")
  private String selectMovieId;

  /**
   * Create modal.
   *
   * @param app the app
   */
  public void createModal(App app) {
    log.info("Registering modal component.");
    app.blockAction(movieButtonId, (req, ctx) -> {
      var logger = ctx.logger;
      try {
        var payload = req.getPayload();
        // Call the conversations.create method using the built-in WebClient
        var modalView = view(v -> v
          .type("modal")
          .title(viewTitle(vt -> vt.type(PlainTextObject.TYPE).text(modalTitle)))
          .submit(viewSubmit(vs -> vs.type(PlainTextObject.TYPE).text(confirmButtonText)))
          .close(viewClose(vc -> vc.type(PlainTextObject.TYPE).text(closeButtonText)))
          .blocks(asBlocks(
            section(s -> s.text(markdownText(mt -> mt.text(selectMovieLabel + ":")))),
            actions(asElements(
              externalSelect(movies ->
                movies.actionId(selectMovieId)
                  .placeholder(plainText(pt -> pt.text(selectMovieLabel)))
                  .minQueryLength(3))
            ))
          ))
        );

        var result = ctx.client().viewsOpen(r -> r
          // The token you used to initialize your app
          .token(System.getenv(Constants.SLACK_BOT_TOKEN))
          .triggerId(payload.getTriggerId())
          .view(modalView)
        );
        // Print result
        log.info("result: {}", result);
      }
      catch (IOException | SlackApiException e) {
        log.error("error: {}", e.getMessage(), e);
      }
      return ctx.ack();
    });

    // Movie selected action
    app.blockAction(selectMovieId, (req, ctx) -> ctx.ack());
  }
}
