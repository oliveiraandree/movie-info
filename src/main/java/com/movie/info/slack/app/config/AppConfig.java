package com.movie.info.slack.app.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.extern.log4j.Log4j2;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import com.movie.info.slack.app.constants.Constants;

/**
 * The type App config.
 */
@Log4j2
@Configuration
public class AppConfig {

  @Value("${tmdb.base-url}")
  private String tmdbBaseUrl;

  /**
   * Web client web client.
   *
   * @return the web client
   */
  @Bean
  public WebClient webClient() {
    WebClient client = WebClient.builder()
      .baseUrl(tmdbBaseUrl)
      .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + System.getenv(Constants.TMDB_ACCESS_TOKEN))
      .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
      .clientConnector(new ReactorClientHttpConnector(tcpClient()))
      .build();
    return client;
  }

  private HttpClient tcpClient() {
    return HttpClient.create()
      .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
      .responseTimeout(Duration.ofMillis(5000))
      .doOnConnected(conn ->
        conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
          .addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));
  }
}
