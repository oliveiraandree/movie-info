package com.movie.info.slack.app.constants;

/**
 * The type Constants.
 */
public class Constants {

  /**
   * The constant DIRECT_MESSAGE_TEMPLATE.
   */
  public static final String DIRECT_MESSAGE_TEMPLATE = "*Release date:* <<RELEASE_DATE>>\n <<DETAILS>>";

  public static final String DM_HEADER = "Here's the movie info you requested.";

  /**
   * The constant IMAGE_URL_BASE.
   */
  public static final String IMAGE_URL_BASE = "https://image.tmdb.org/t/p/w92/";

  /**
   * The constant SLACK_BOT_TOKEN.
   */
  public static final String SLACK_BOT_TOKEN = "SLACK_BOT_TOKEN";
  public static final String TMDB_ACCESS_TOKEN = "TMDB_ACCESS_TOKEN";
}
