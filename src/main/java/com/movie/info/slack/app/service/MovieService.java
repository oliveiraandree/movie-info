package com.movie.info.slack.app.service;

import info.movito.themoviedbapi.model.MovieDb;

import java.util.List;

import com.slack.api.app_backend.interactive_components.response.Option;

public interface MovieService {
  List<Option> loadMovies(String input);

  MovieDb getDetails(Integer movieId);
}
