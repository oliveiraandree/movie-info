package com.movie.info.slack.app.service.impl;

import static com.slack.api.model.block.composition.BlockCompositions.plainText;

import info.movito.themoviedbapi.model.MovieDb;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.movie.info.slack.app.client.TheMovieDBClient;
import com.movie.info.slack.app.service.MovieService;
import com.slack.api.app_backend.interactive_components.response.Option;

@Log4j2
@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

  private final TheMovieDBClient client;

  @Override
  public List<Option> loadMovies(String input) {
    log.info("load movies - start - input: {}", input);
    List<MovieDb> movieDbs = client.loadMovies(input);
    return convertMoviesToOptionObject(movieDbs);
  }

  @Override
  public MovieDb getDetails(Integer movieId) {
    log.info("get details - movieId: {}", movieId);
    return client.getMovieDetails(movieId);
  }

  private List<Option> convertMoviesToOptionObject(List<MovieDb> movies) {
    List<Option> optionObjects =
      movies.stream().sorted(Comparator.comparing(MovieDb::getOriginalTitle))
        .map(movieDb -> Option.builder().text(plainText(movieDb.getOriginalTitle()))
          .value(String.valueOf(movieDb.getId())).build()).collect(Collectors.toList());
    log.info("movies converted - {}", optionObjects);
    return optionObjects;
  }
}
