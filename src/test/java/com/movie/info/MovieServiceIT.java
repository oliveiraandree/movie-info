package com.movie.info;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import info.movito.themoviedbapi.model.MovieDb;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.movie.info.slack.app.service.MovieService;
import com.slack.api.app_backend.interactive_components.response.Option;

@SpringBootTest
public class MovieServiceIT {

  public static final int MOVIE_ID = 671;
  public static final String INPUT = "Star";
  public static final String MOVIE_ORIGINAL_TITLE = "Harry Potter and the Philosopher's Stone";

  @Autowired
  private MovieService movieService;

  /**
   * Testing integration from Movie Service and TMDB Api
   */
  @Test
  public void loadMoviesTest(){
    List<Option> movies = movieService.loadMovies(INPUT);
    assertThat(movies, Matchers.hasSize(20));
  }

  /**
   * Testing integration from Movie Service and TMDB Api
   */
  @Test
  public void getMovieDetailTest(){
    MovieDb movieDb = movieService.getDetails(MOVIE_ID);
    assertThat(movieDb, notNullValue());
    assertEquals(MOVIE_ORIGINAL_TITLE, movieDb.getOriginalTitle());
  }

}
