package com.movie.info.slack.app.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import info.movito.themoviedbapi.model.MovieDb;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.movie.info.slack.app.client.TheMovieDBClient;
import com.movie.info.slack.app.service.MovieService;
import com.slack.api.app_backend.interactive_components.response.Option;

@ExtendWith(MockitoExtension.class)
class MovieServiceImplTest {

  private MovieService movieService;

  @Mock
  private TheMovieDBClient theMovieDBClient;

  @BeforeEach
  public void setup(){
    movieService = new MovieServiceImpl(theMovieDBClient);
  }

  @Test
  public void loadingOneMovieTest(){
    String input  = "Star";
    when(theMovieDBClient.loadMovies(input)).thenReturn(Collections.singletonList(mock(MovieDb.class)));
    List<Option> movies = movieService.loadMovies(input);
    assertThat(movies, hasSize(1));
  }

  @Test
  public void clientReturningNoMoviesTest(){
    String input  = "Star";
    when(theMovieDBClient.loadMovies(input)).thenReturn(Collections.EMPTY_LIST);
    List<Option> movies = movieService.loadMovies(input);
    assertThat(movies, empty());
  }

  @Test
  public void clientThrowingExceptionMovieTest(){
    String input  = "Star";
    when(theMovieDBClient.loadMovies(input)).thenThrow(RuntimeException.class);
    assertThrows(RuntimeException.class, () -> movieService.loadMovies(input));
  }


  @Test
  public void getMovieDetailsTest(){
    Integer movieId  = 1;
    MovieDb movieDbMock = mock(MovieDb.class);
    when(theMovieDBClient.getMovieDetails(movieId)).thenReturn(movieDbMock);
    MovieDb movieDb = movieService.getDetails(movieId);
    assertThat(movieDb, notNullValue());
    assertEquals(movieDbMock.getOriginalTitle(), movieDb.getOriginalTitle());
    assertEquals(movieDbMock.getReleaseDate(), movieDb.getReleaseDate());
    assertEquals(movieDbMock.getOverview(), movieDb.getOverview());
    assertEquals(movieDbMock.getId(), movieDb.getId());
  }

  @Test
  public void getMovieDetailThrowingExceptionTest(){
    Integer movieId  = 1;
    when(theMovieDBClient.getMovieDetails(movieId)).thenThrow(RuntimeException.class);
    assertThrows(RuntimeException.class, () -> movieService.getDetails(movieId));
  }
}